'''
__author__      = "Rajkumar Venkataraman"
__copyright__   = "Copyright 2019, Rkoots"
__credits__ = []
__license__ = "GPL"
__version__ = "1.0.1"
__maintainer__ = "Rajkumar Venkataraman"
__email__ = "rkootsindia@gmail.com"
__status__ = "Production"
Converts PDF text content (though not images containing text) to plain text, html, xml or "tags".

'''
from .pdf2table_api import (Client,
                            APIException,
                            FORMAT_CSV,
                            FORMAT_XLSX,
                            FORMAT_XLSX_MULTIPLE,
                            FORMAT_XLSX_SINGLE,
                            FORMAT_XML)
