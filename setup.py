#!/usr/bin/env python

'''
__author__      = "Rajkumar Venkataraman"
__copyright__   = "Copyright 2019, Rkoots"
__credits__ = []
__license__ = "GPL"
__version__ = "1.0.1"
__maintainer__ = "Rajkumar Venkataraman"
__email__ = "rkootsindia@gmail.com"
__status__ = "Production"
Converts PDF text content (though not images containing text) to plain text, html, xml or "tags".

'''
import os
from setuptools import setup


def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


setup(
    name = 'pdf2table-api',
    version = '1.1.0',
    author = 'The Sensible Code Company',
    author_email = 'rkoots@gmail.com',
    description = ('PDF Python API library.'),
    long_description=read('README.md'),
    license = 'OpenSource',
    keywords = 'pdf tables excel csv xml api',
    packages=['pdf2table_api'],
    install_requires=[
        'requests',
    ],
    tests_require=[
        'requests_mock',
    ],
    classifiers=[
        'Development Status :: Production/Stable',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: Apache Software License',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 3',
        'Topic :: Software Development :: Libraries',
        'Topic :: System :: Networking',
    ],
)
